// api accessKey generated from unsplash

const accessKey = "hCSJf2bKzjsLuEx5ALCn6D_VXWzFIjOfkv0CUaPTHYc"

// imported all the necessary elements from index.html

const formElement = document.querySelector("form")
const inputElement = document.getElementById("search-input")
const searchResults = document.querySelector(".search-results")
const showMore = document.getElementById("show-more-button")

let inputData = ""   // the inputData will be the keyword ie. entering in the input field
let page = 1 ;        // corresponding page number


// async function is using because we have to use the "response and fetch"
async function searchImages(){
    inputData = inputElement.value;   // keyword enetring to input element is intialised to inputData
    const url = `https://api.unsplash.com/search/photos?page=${page}&query=${inputData}
    &client_id=${accessKey}`  // based on the keyword image, pagenumber, is fetched from this api

    const response = await fetch(url) 
    const data = await response.json() // then that data is converted to json format 

    const results = data.results  // json - image & anchor text

    // intailize the page number

    if (page === 1){
        searchResults.innerHTML = "";
    }

    // now inside const results  variable there are lot of images and lot of data and 
    // therefore we need to show the images and text one by one

    // so we need to map these results with search result container("div")

    results.map((result ) =>{
        const imageWrapper = document.createElement('div');
        imageWrapper.classList.add("search-result");
        const image = document.createElement('img');
        image.src = result.urls.small;
        image.alt = result.alt_description;
        const imagelink = document.createElement('a');
        imagelink.href = result.links.html;
        imagelink.target = "_blank";
        imagelink.textContent = result.alt_description;


        imageWrapper.appendChild(image);
        imageWrapper.appendChild(imagelink);
        searchResults.appendChild(imageWrapper);
    })


    page++ // to incresae the page number inacase of more images have to be displayed
    if (page > 1){
        showMore.style.display = "block"  ///later in the css we setted the display style as none inorder not to display show more button
    }                                     /// so to display show more button if the response is been created... --> style ( display changed to block )
}

// this entire function wont work until eventlistner is created ... so we have to add addEventListener 
// so that if a query is generated for search image is generated event listner will do the searchImages() -- function

formElement.addEventListener("submit",(event) => {
    event.preventDefault()
    page = 1;
    searchImages()
})

showMore.addEventListener("click",() =>{
    searchImages()
})

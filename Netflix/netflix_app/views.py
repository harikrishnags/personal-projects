
# netflix_app/views.py
from django.shortcuts import render

def index(request):
    return render(request, 'netflix_app/index.html')
